package com.galvanize;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class VerifierTest {

    private Verifier verifier;

    @BeforeEach
    void setUp() {
    }

    @Test
    void verify(final String zipCode) throws InvalidFormatException, NoServiceException {
        verifier = new Verifier();
        assertThrows(InvalidFormatException.class, () -> verifier.verify("999"));
        assertThrows(NoServiceException.class, () -> verifier.verify("10304"));

        //assertEquals("ERRCODE 21: INPUT_TOO_LONG", verifier.getMessage(zipCode));
        //assertEquals("ERRCODE 22: INPUT_TOO_SHORT", verifier.getMessage(zipCode));
        //assertEquals("ERRCODE 22: NO_SERVICE", verifier.getMessage(zipCode));

    }

//   void getMessage(String zipCode){
//        verifier = new Verifier();
//        String actual = verifier.getMessage("9999999");
//        String expected = "ERRCODE 21: INPUT_TOO_LONG";
//
//        assertEquals(expected, actual);
//
//    }
}
