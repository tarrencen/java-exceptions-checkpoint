package com.galvanize;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ZipCodeProcessorTest {

    private Verifier verifier;
    private ZipCodeProcessor zipCodeProcessor;

    @BeforeEach
    void setUp() {
        this.verifier = verifier;
        this.zipCodeProcessor = zipCodeProcessor;
    }

    // write your tests here
    @Test
    void process(String zipCode) throws InvalidFormatException, NoServiceException {
        //Arrange
        verifier = new Verifier();
        zipCodeProcessor = new ZipCodeProcessor(verifier);
        //Act
        String actual = zipCodeProcessor.process("10304");
        //Assert
        String expected = "We're sorry, but the zipcode you entered is out of our range.";
        assertEquals(expected, actual);

    }


}